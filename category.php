<?php
    get_template_part('includes/header'); 
    themefn_main_before();
?>

<main id="main" class="container mt-5">
  <div class="row">

    <div class="col-sm">
      <div id="content" role="main">
        <header class="mb-4 border-bottom">
          <h1>
            <?php _e('Category: ', 'themefn'); echo single_cat_title(); ?>
          </h1>
        </header>
        <?php get_template_part('includes/loops/index-loop'); ?>
      </div><!-- /#content -->
    </div>

    <?php get_template_part('includes/sidebar'); ?>

  </div><!-- /.row -->
</main><!-- /.container -->

<?php 
    themefn_main_after();
    get_template_part('includes/footer'); 
?>
