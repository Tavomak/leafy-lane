<?php
/*
 * themefn Hooks
 * Designed to be used by a child theme.
 */

// Navbar (in `header.php`)

function themefn_navbar_before() {
  do_action('navbar_before');
}

function themefn_navbar_after() {
  do_action('navbar_after');
}
function themefn_navbar_brand() {
  if ( ! has_action('navbar_brand') ) {
    ?>
    <a class="navbar-brand" href="<?php echo esc_url( home_url('/') ); ?>"><?php bloginfo('name'); ?></a>
    <?php
  } else {
		do_action('navbar_brand');
	}
}
function themefn_navbar_search() {
  if ( ! has_action('navbar_search') ) {
    ?>
    <form class="form-inline ml-auto pt-2 pt-md-0" role="search" method="get" id="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
      <div class="input-group">
        <input class="form-control border-secondary" type="text" value="<?php echo get_search_query(); ?>" placeholder="Search..." name="s" id="s">
        <div class="input-group-append">
          <button type="submit" id="searchsubmit" value="<?php esc_attr_x('Search', 'themefn') ?>" class="btn btn-outline-secondary">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>
    <?php
  } else {
		do_action('navbar_search');
	}
}


// Main

function themefn_main_before() {
  do_action('main_before');
}
function themefn_main_after() {
  do_action('main_after');
}

// Sidebar (in `sidebar.php` -- only displayed when sidebar has 1 widget or more)

function themefn_sidebar_before() {
  do_action('sidebar_before');
}
function themefn_sidebar_after() {
  do_action('sidebar_after');
}

// Footer (in `footer.php`)

function themefn_footer_before() {
  if ( ! has_action('footer_before')) {
  ?>
    <section class="container-fluid pt-5 pb-5 lf-footer-before">
      <div class="container">
        <div class="row pt-3">
          <div class="col-sm">
            <h4 class=""><i class="far fa-envelope"></i> It’s your life. We’re here to help you live it well.</h4>
            <p>Join the Leafy Lane club and instantly get stories, health guides and discounts every week.</p>
            <?php echo do_shortcode('[contact-form-7 id="60" title="Newsletter"]');?>
          </div>
          <div class="col-sm">
          </div>
        </div>
      </div>
    </section>
  <?php
  }else {
    do_action('footer_before');
  }
}
function themefn_footer_after() {
  do_action('footer_after');
}
function themefn_bottomline() {
	if ( ! has_action('bottomline') ) {
		?>
    <section class="container">
      <div class="row pt-3">
        <div class="col-sm">
          <p class=""><small>*These statements have not been evaluated by the Food and Drug Administration. <br>*This product is not intended to diagnose, treat, cure or prevent any disease.</small></p>
        </div>
        <div class="col-sm">
            <p class=" text-center text-sm-right"><small>&copy; <?php echo date('Y'); ?> <a href="<?php echo home_url('/'); ?>" class="text-white"><?php bloginfo('name'); ?></a></small></p>
        </div>
      </div>
    </section>
		<?php		
	} else {
		do_action('bottomline');
	}
}
