<?php
    get_template_part('includes/header'); 
    themefn_main_before();
    echo do_shortcode('[rev_slider alias="home-slider"]');
?>
<section class="container">
      <div id="content" role="main" class="">
        <?php if(have_posts()): while(have_posts()): the_post(); ?>
        
        <?php the_content()?>

        <?php
          endwhile;
          else :
            get_template_part('includes/loops/404');
          endif;
        ?>  
      <?php //get_template_part('includes/sidebar'); ?>
  
    </div> 
</section>

<!-- <section class="container-fluid lf-story pt-5 pb-5">
  <div class="container">
    <div class="row align-items-center mb-5">
  
      <div class="col-md-6">
        <img src="<?php bloginfo('template_directory'); ?>/assets/img/copia.png" alt="Leafy Lane">
      </div>
      <div class="col-md-6">
        <div class="pr-2">
          <h4>We believe in the power <br>of nature to help you heal</h4>
          <p>You know pharmaceuticals aren’t the only way. You deserve an easy way to take your health back into your own hands. But how do you know which products are right for you? That’s where we come in. We believe that ethically-grown, sustainable alternatives for health are good for you and the planet… as long as they’re rigorously tested first.</p>
        </div>
      </div>
    </div>
  
    <div class="row align-items-center">
      <div class="col-md-6">
        <div class="pr-2">
          <h4>Your path to a happier, <br>healthier you starts here</h4>
          <p>What began as a way to leverage our science background for the greater good has developed into the most trusted ecommerce website for CBD and high-quality hemp products. <br>We’re geeks for green whose mission is <br>your health and happiness.</p>
          <button>OUR STORY</button>
        </div>
      </div>
      <div class="col-md-6">
        <img class="w-100" src="<?php bloginfo('template_directory'); ?>/assets/img/copia.png" alt="Leafy Lane">
      </div>
    </div>
  </div>
</section>

<section class="container-fluid pt-5 pb-5 lf-shop-section">
  <div class="container">
  <div class="row">
    <div class="col text-center">
      <h2>All natural health. All natural healing.</h2>
      <p>The best natural alternatives curated for you</p>
      <p><a href="">All products</a></p>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-4">
      <article class="porduct-card p-5">
        <div >
          <div class="text-center">
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/product.png" alt="">
          </div>
        </div>
        <div>
          <h4>Full Strength Hemp Extract</h4>
          <p>Hemp oil with CBD</p>
          <img src="<?php bloginfo('template_directory'); ?>/assets/img/valoration.png" alt="">
          <p class="price">$39.99 - $99.99</p>
          <button class="btn">SHOP</button>
        </div>
      </article>
    </div>
    <div class="col-sm-4">
      <article class="porduct-card p-5">
        <div >
          <div class="text-center">
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/product.png" alt="">
          </div>
        </div>
        <div>
          <h4>Full Strength Hemp Extract</h4>
          <p>Hemp oil with CBD</p>
          <img src="<?php bloginfo('template_directory'); ?>/assets/img/valoration.png" alt="">
          <p class="price">$39.99 - $99.99</p>
          <button class="btn">SHOP</button>
        </div>
      </article>
    </div>
    <div class="col-sm-4">
    <article class="porduct-card p-5">
        <div >
          <div class="text-center">
            <img src="<?php bloginfo('template_directory'); ?>/assets/img/product.png" alt="">
          </div>
        </div>
        <div>
          <h4>Full Strength Hemp Extract</h4>
          <p>Hemp oil with CBD</p>
          <img src="<?php bloginfo('template_directory'); ?>/assets/img/valoration.png" alt="">
          <p class="price">$39.99 - $99.99</p>
          <button class="btn">SHOP</button>
        </div>
      </article>
    </div>
  </div>
  </div>
</section>

<section class="container-fluid lf-testimonial pt-5 pb-5">
  <div class="container">
    <div class="row">
      <div class="col text-center">
        <h2 class="">Here’s what they’re saying on Leafy Lane:</h2>
        <p><a href="">All reviews</a></p>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-4">
        <article class="p-5 testimonial-card">
            <p>Interdum et malesuada ac ante ipsum primis in faucibus. Cras libero sapien, dictum eu elementum sit amet, vehicula ut ipsum. Proin placerat faucibus turpis.</p>
            <p><strong>Maria Haller</strong></p>
            <p><small>Сompleted courses in logic</small></p>
        </article>
      </div>
      <div class="col-sm-4">
        <article class="p-5 testimonial-card">
            <p>Interdum et malesuada ac ante ipsum primis in faucibus. Cras libero sapien, dictum eu elementum sit amet, vehicula ut ipsum. Proin placerat faucibus turpis.</p>
            <p><strong>Maria Haller</strong></p>
            <p><small>Сompleted courses in logic</small></p>
        </article>
      </div>
      <div class="col-sm-4">
        <article class="p-5 testimonial-card">
            <p>Interdum et malesuada ac ante ipsum primis in faucibus. Cras libero sapien, dictum eu elementum sit amet, vehicula ut ipsum. Proin placerat faucibus turpis.</p>
            <p><strong>Maria Haller</strong></p>
            <p><small>Сompleted courses in logic</small></p>
        </article>
      </div>
    </div>
  </div>
</section>-->

  <?php
  $custom_query = new WP_Query(array(
    'post_type'       => 'experience',
    'posts_per_page'  => 4,
    'post_status'     => 'publish'
  ));
  if ( $custom_query->have_posts() ) :
  ?>
<section class="container pt-5 pb-5">
  <div class="row">
    <div class="col text-center">
      <h2 class="pb-5">Learn more about your journey</h2>
    </div>
  </div>
  <div class="row">
  <?php while( $custom_query->have_posts() ) : $custom_query->the_post(); ?>
    <div class="col-sm-3">
    <article class="blog-card">
        <div >
          <img src="<?php the_post_thumbnail_url('medium'); ?>" alt="">
        </div>
        <div>
          <h4>
          <?php echo get_the_title();?>
          </h4>
          <p>
          <?php echo get_the_excerpt();?>
          </p>
        </div>
      </article>
    </div>
    <?php endwhile;
      wp_reset_postdata();?>
  </div>
</section> 
<?php endif;?>

<?php 
    themefn_main_after();
    get_template_part('includes/footer'); 
?>
