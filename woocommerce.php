<?php
    get_template_part('includes/header'); 
    themefn_main_before();
    if (is_product_category('cbd-gummies')){
        //echo do_shortcode('[rev_slider alias="home-slider"]');
    }
?>

<div class="bk-woocommerce container">
  <?php woocommerce_content(); ?>
</div>


<?php 
    themefn_main_after();
    get_template_part('includes/footer'); 
?>
