<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php themefn_navbar_before();?>
<div class="cd-loader">
  <div class="cd-loader__grid">
    <div class="cd-loader__item"></div>
  </div>
</div>
<section class="preheader container">
  <div class="row">
    <div class="col-6"><p><small>Free Shipping on Orders of $100 & Up!</small></p></div>
    <div class="col-6">
      <ul class="d-flex justify-content-between align-items-center">
        <li><i class="fas fa-shopping-cart"></i> <i class="fas fa-search"></i></li>
        <li><p><small><a href="#woo-login-popup-sc-register" class="woo-login-popup-sc-open">SING IN</a><span> OR </span><a href="" class="woo-login-popup-sc-open">REGISTER</a></small></p></li>
      </ul>
    </div>
  </div>
</section>

<nav id="navbar" class="navbar navbar-expand-md navbar-light bg-light">
  <div class="container d-flex align-items-baseline">

    <?php //themefn_navbar_brand();?>
    <a href="<?php echo esc_url( home_url('/') ); ?>">
      <img src="<?php bloginfo('template_directory'); ?>/assets/img/logo.png" alt="Leafy Lane">
    <p class="text-center mb-0"><small>A healthier you — without the high.</small></p>
    </a>

    <button class="hamburger hamburger--emphatic navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarDropdown" aria-controls="navbarDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="hamburger-box">
        <span class="hamburger-inner"></span>
      </span>
    </button>

    <div class="collapse navbar-collapse justify-content-end" id="navbarDropdown">
      <?php
        wp_nav_menu( array(
          'theme_location'  => 'navbar',
          'container'       => false,
          'menu_class'      => '',
          'fallback_cb'     => '__return_false',
          'items_wrap'      => '<ul id="%1$s" class="navbar-nav %2$s">%3$s</ul>',
          'depth'           => 2,
          'walker'          => new themefn_walker_nav_menu()
        ) );
      ?>

      <?php //themefn_navbar_search();?>    
    </div>

  </div>
</nav>
<section class="container-fluid bg-light">
<ul class="container d-flex align-items-center">
  <li class="pr-2"><p><small>Shop by <br>Concern</small></p></li>
  <li class="pr-2"><p><small><a href="<?php echo site_url('/pure-cbd');?>"><img src="<?php bloginfo('template_directory'); ?>/assets/img/green.png" alt=""> Pure CBD</a></small></p></li>
  <li class="pr-2"><p><small><a href="<?php echo site_url('/mood-boost');?>"><img src="<?php bloginfo('template_directory'); ?>/assets/img/green.png" alt=""> Mood Boost</a></small></p></li>
  <li class="pr-2"><p><small><a href="<?php echo site_url('/pain-relief');?>"><img src="<?php bloginfo('template_directory'); ?>/assets/img/blue.png" alt=""> Pain Relief</a></small></p></li>
  <li class="pr-2"><p><small><a href="<?php echo site_url('/anxiety-relief');?>"><img src="<?php bloginfo('template_directory'); ?>/assets/img/yellow.png" alt=""> Anxiety Relief</a></small></p></li>
  <li class="pr-2"><p><small><a href="<?php echo site_url('/beauty-self-care');?>"><img src="<?php bloginfo('template_directory'); ?>/assets/img/yellow.png" alt=""> Beauty and Self-care</a></small></p></li>
</ul>
</section>
<?php themefn_navbar_after();?>
<main class="main-container">