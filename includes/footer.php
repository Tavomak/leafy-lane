</main><!-- /.container -->
<?php themefn_footer_before();?>

<footer id="footer" class="lf-footer">

  <div class="container">
    <div class="row pt-5 pb-4" role="navigation">

    <div class="col-sm-4">
      <img src="<?php bloginfo('template_directory'); ?>/assets/img/footer-logo.png" alt="Leafy Lane">
      <p><strong>Natural CBD products that work.</strong><br>Nature-made. Plant-based. Science-backed.</p>
    </div>
    <div class="col-sm-4">
      <?php if(is_active_sidebar('footer-widget-area')): ?>
        <?php dynamic_sidebar('footer-widget-area'); ?>
      <?php endif; ?>
    </div>
    <div class="col-sm-4">
    <p><strong>JOIN THE Leafy Lane COMMUNITY</strong></p>
    <p>
      <img src="<?php bloginfo('template_directory'); ?>/assets/img/fb.png" alt="Leafy Lane Facebook">
      <img src="<?php bloginfo('template_directory'); ?>/assets/img/gm.png" alt="Leafy Lane Gmail">
      <img src="<?php bloginfo('template_directory'); ?>/assets/img/tw.png" alt="Leafy Lane Twitter">
    </p>
    <p><strong>CALL US</strong></p>
    <h4>8 (800) 1233211</h4>
    <p><small>PO Box 16122 Collins Street West <br>Miami, FL</small></p>
    </div>

      
    </div>
  </div>

  <?php themefn_bottomline();?>
</footer>


<?php themefn_footer_after();?>

<?php wp_footer(); ?>
</body>
</html>
