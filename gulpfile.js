const
  gulp = require('gulp'),
  bourbon = require("bourbon").includePaths,
  browsersync = require('browser-sync').create(), //Recarga el navegador
  autoprefixer = require('gulp-autoprefixer'), //Autoprefixer
  notify = require('gulp-notify'), //Notificaciones
  sass = require('gulp-sass'), //Copila SCSS
  sourcemaps = require('gulp-sourcemaps'), //SourceMaps
  uglify = require('gulp-uglify'), //Minifica JS
  cleanCSS = require('gulp-clean-css'), //Minifica CSS
  rename = require('gulp-rename'), //Renombra archivos
  ftp = require('vinyl-ftp'); //Minifica JS
//concat = require('gulp-concat'), //Concatena archivos js (hay ubo para css)
//gutil = require('gulp-util'),

// BrowserSync
function browserSync(done) {
  browsersync.init({
    proxy: "http://localhost/leafylane",
    port: 8080
  });
  done();
}

// BrowserSync Reload
function browserSyncReload(done) {
  browsersync.reload();
  done();
}


// Define tasks after requiring dependencies
function scss() {
  return (
    gulp
    .src('./assets/scss/main.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({
      outputStyle: 'expanded',
      sourceComments: true,
      includePaths: [bourbon]
    }))
    .on("error", notify.onError({
      sound: true,
      title: 'Error de copilación SCSS'
    }))
    .pipe(autoprefixer({
      versions: ['last 2 browsers']
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('./assets/css/'))
    .pipe(browsersync.stream())
  );
}


// Watch files
function watchFiles() {
  gulp.watch("./assets/scss/**/*", scss);
  gulp.watch("./assets/js/**/*", browserSyncReload);
  gulp.watch(
    [
      "./_includes/**/*",
      "./_layouts/**/*",
      "./_pages/**/*",
      "./_posts/**/*",
      "./_projects/**/*"
    ],
    gulp.series(browserSyncReload)
  );
  gulp.watch("./**/*.php", browserSyncReload);
}

// define complex tasks
const watch = gulp.parallel(watchFiles, browserSync);

// export tasks
exports.scss = scss;
exports.watch = watch