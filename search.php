<?php
    get_template_part('includes/header'); 
    themefn_main_before();
?>

  <div class="row">

    <div class="col-sm">
      <div id="content" role="main">
        <header class="mb-4 border-bottom">
          <h1>
            <?php _e('Search Results for', 'themefn'); ?> &ldquo;<?php the_search_query(); ?>&rdquo;
          </h1>
        </header>
        <?php get_template_part('includes/loops/search-results'); ?>
      </div><!-- /#content -->
    </div>

    <?php get_template_part('includes/sidebar'); ?>

  </div><!-- /.row -->

<?php 
    themefn_main_after();
    get_template_part('includes/footer'); 
?>
