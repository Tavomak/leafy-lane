<?php
    get_template_part('includes/header'); 
    themefn_main_before();
?>

  <div class="row">

    <div class="col-sm">
      <div id="content" role="main">

        <?php get_template_part('includes/loops/index-loop'); ?>

      </div><!-- /#content -->
    </div>

    <?php //get_template_part('includes/sidebar'); ?>

  </div><!-- /.row -->


<?php 
    themefn_main_after();
    get_template_part('includes/footer'); 
?>
